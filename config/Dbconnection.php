<?php

class Dbconnection
{
    private $host = 'localhost';
    private $username = 'root';
    private $password = 'root';
    private $db = 'otrium_challenge';
    private $connection = null;

    function getDbConnect()
    {
        $con = mysqli_connect($this->host, $this->username, $this->password, $this->db) or die("Couldn't connect");
        return $con;
    }

    // this function is called everytime this class is instantiated
    public function __construct()
    {

        try {

            $this->connection = new PDO("mysql:host={$this->host};dbname={$this->db};", $this->username, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Select a row, true/s in a Database Table
    public function Select($statement = "", $parameters = [])
    {
        try {

            $stmt = $this->executeStatement($statement, $parameters);
            return $stmt->fetchAll();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // execute statement
    private function executeStatement($statement = "", $parameters = [])
    {
        try {

            $stmt = $this->connection->prepare($statement);
            $stmt->execute($parameters);
            return $stmt;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
