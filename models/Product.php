<?php

require_once('config/Dbconnection.php');

class Product
{

    function getBrandDetails()
    {
        $db = new Dbconnection();
        $result = $db->Select("SELECT brands.name, gmv.brand_id, gmv.date, gmv.turnover FROM brands INNER JOIN gmv ON brands.id = gmv.brand_id WHERE gmv.date BETWEEN '2018-05-01' AND '2018-05-07'");
        return $result;
    }
}
