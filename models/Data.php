<?php

class Data
{
    function dayTurnOver($data)
    {
        $dayResults = array();
        $total = 0;
        foreach ($data as $val) {
            if (array_key_exists('date', $val)) {
                $dayResults[$val['date']]['date'] = $val['date'];
                $dayResults[$val['date']][$val['name']] = $val['turnover'];
                $dayResults[$val['date']]['total'] = $total;
                $total = $dayResults[$val['date']]['total'] + $val['turnover'];
                unset($dayResults[$val['date']]['total']);
                $dayResults[$val['date']]['total'] = $total - ($total * 0.21);
            } else {
                $dayResults[''][] = $val;
            }
        }
        return $dayResults;
    }

    function weekTurnOver($data)
    {
        $weekResults = array();
        $total = 0;
        foreach ($data as $val) {
            if (array_key_exists('name', $val)) {
                $weekResults[$val['name']]['brand'] = $val['name'];
                $weekResults[$val['name']][$val['date']] = $val['turnover'];
                $weekResults[$val['name']]['total'] = $total;
                $total = $weekResults[$val['name']]['total'] + $val['turnover'];
                unset($weekResults[$val['name']]['total']);
                $weekResults[$val['name']]['total'] = $total - ($total * 0.21);
            } else {
                $weekResults[''][] = $val;
            }
        }
        return $weekResults;
    }
}
