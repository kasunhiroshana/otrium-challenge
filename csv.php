<?php

function csv($array, $fileName)
{

    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $file = fopen('assets/csv/' . $fileName . '.csv', 'w');

    fputcsv($file, array_keys(reset($array)));

    foreach ($array as $row) {
        fputcsv($file, $row);
    }

    fclose($file);
    return ob_get_clean();
}
