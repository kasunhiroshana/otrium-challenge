# Otrium challenge #

### Connecting DB ###

Import given database and change mysql login details in Dbconnection.php in config folder

ex: 

* host = 'localhost'
* username = 'root'
* password = 'root'
* db = 'otrium_challenge'

### CSV ###

CSV files are generated in assets > csv folder

* day-turnover.csv for 7 days turnover per day. (Day, total turnover(excluding VAT) per day) 
* week-turnover.csv for 7 days turnover per brand (Brand Name, Total turnover(excluding VAT) per day for last 7 days) 