<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('models/Data.php');
require_once('models/Product.php');
require_once('csv.php');

$product = new Product();
$productData = $product->getBrandDetails();

$data = new Data();
$dayResult = $data->dayTurnOver($productData);
$weekResult = $data->weekTurnOver($productData);

csv($weekResult, 'week-turnover'); //Week turnover csv
csv($dayResult, 'day-turnover'); //Day turnover csv
